# Simples forma de melhorar o condicionador de ar veicular 

Saber qual a tubulação de refrigeração do veículo e seu percurso.

Comprar espuma isolante para tubulações de condicionador de ar de acordo com a espessura localizada no seu veículo.

Isolar toda essa espuma com fita de alumínio.

Recortar, colar a espuma na tubulação e prende com presilhas de nylon.

Esse processo aumenta um pouco a capacidade de refrigeração do ar condicionador veicular.

Para saber exatamente o aumento, deves fazer alguns cálculos.
